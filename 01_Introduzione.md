---
author: Luca Lanteri/Rocco Pispico
title: Introduzione a SQL con PostgreSQL
date: Novembre, 2023
Sede: Arpa Piemonte
---

# Corso PostgreSQL / PostGIS

## Introduzione

Il corso è articolato in 3 diversi livelli:

**LIVELLO BASE:** *Introduzione al linguaggio SQL con PostgreSQL*. E' mirato al personale che non ha confidenza con il linguaggio SQL. Ha il fine di fornire le conoscenze di base per poter interagire con i database relazionali. Le conoscenze acquisite possono essere applicate a PostgreSQL ma anche a qualsiasi altro database. A parte alcune particolarità dialettiche tipiche di PostgreSQL di tratta si SQL ANSI/ISO standard.

**LIVELLO INTERMEDIO:** *Gestione e analisi dati con PostgreSQL.* E' necessaria la conoscenza di base dell'SQL. Vengono affrontate funzioni di analisi e gestione più avanzate, come ad esempio query complesse, il caricamento dati da fonti esterne e l'utilizzo di PostgreSQL in ambienti differenti.

**LIVELLO AVANZATO:** *PostGIS - gestione dei dati spaziali con PostgreSQL.* Finalizzato a chi ha già acquisito le conoscenze dei corsi precedenti. Gestione della componente geografica all'interno di un geodatabase open source.



## SQL - Un po' di storia

In [informatica](https://it.wikipedia.org/wiki/Informatica), **SQL** (**Structured Query Language**) (/skjuːˈɛl/ "S-Q-L") è un linguaggio [standardizzato](https://it.wikipedia.org/wiki/Standard_(informatica)) per [database](https://it.wikipedia.org/wiki/Database) basati sul [modello relazionale](https://it.wikipedia.org/wiki/Modello_relazionale) ([RDBMS](https://it.wikipedia.org/wiki/Relational_database_management_system)), (Relational database management system) progettato per le seguenti operazioni:

- creare e modificare [schemi di database](https://it.wikipedia.org/wiki/Schema_di_database) (DDL = *[Data Definition Language](https://it.wikipedia.org/wiki/Data_Definition_Language)*);
- inserire, modificare e gestire dati memorizzati (DML = *[Data Manipulation Language](https://it.wikipedia.org/wiki/Data_Manipulation_Language)*);
- interrogare i dati memorizzati (DQL = *[Data Query Language](https://it.wikipedia.org/wiki/Query_language)*);
- creare e gestire strumenti di controllo e accesso ai dati (DCL = *[Data Control Language](https://it.wikipedia.org/wiki/Data_Control_Language)*).

A dispetto del nome, non si tratta perciò di un semplice [linguaggio di interrogazione](https://it.wikipedia.org/wiki/Query_language): alcuni suoi sottoinsiemi, infatti, permettono di creare, gestire e amministrare database.[[1\]](https://it.wikipedia.org/wiki/Structured_Query_Language#cite_note-1)

l SQL nasce nel [1974](https://it.wikipedia.org/wiki/1974)  nei laboratori dell'[IBM](https://it.wikipedia.org/wiki/IBM). Nasce come strumento per lavorare con [database](https://it.wikipedia.org/wiki/Database) che seguano il [modello relazionale](https://it.wikipedia.org/wiki/Modello_relazionale). A quel tempo però si chiamava [SEQUEL](https://it.wikipedia.org/w/index.php?title=SEQUEL&action=edit&redlink=1) 

L'[ANSI](https://it.wikipedia.org/wiki/ANSI) lo adottò come standard fin dal [1986](https://it.wikipedia.org/wiki/1986), senza apportare modifiche sostanziali alla versione inizialmente sviluppata da IBM. Nel [1987](https://it.wikipedia.org/wiki/1987) la [ISO](https://it.wikipedia.org/wiki/ISO) fece lo stesso. Tale processo di standardizzazione mirava alla creazione di un linguaggio che funzionasse su tutti i DBMS (Data Base Management Systems)  relazionali, ma questo obiettivo non fu raggiunto. Infatti, i vari  produttori implementarono il linguaggio con numerose variazioni e, in  pratica, adottarono gli standard ad un livello non superiore al minimo,  definito dall'Ansi come Entry Level.

SQL è un linguaggio per interrogare e gestire basi di dati mediante l'utilizzo di costrutti di programmazione denominati [query](https://it.wikipedia.org/wiki/Query). Con SQL si leggono, modificano, cancellano dati e si esercitano  funzioni gestionali ed amministrative sul sistema dei database. La  maggior parte delle implementazioni dispongono di [interfaccia](https://it.wikipedia.org/wiki/Interfaccia_(informatica)) alla [riga di comando](https://it.wikipedia.org/wiki/Riga_di_comando) per l'esecuzione diretta di comandi, in alternativa alla sola interfaccia grafica [GUI](https://it.wikipedia.org/wiki/GUI).

Originariamente progettato come linguaggio di tipo più semplice, si è successivamente evoluto con l'introduzione di costrutti [procedurali](https://it.wikipedia.org/wiki/Programmazione_procedurale), istruzioni per il [controllo di flusso](https://it.wikipedia.org/wiki/Controllo_di_flusso), [tipi di dati](https://it.wikipedia.org/wiki/Tipo_di_dato) definiti dall'utente e varie altre estensioni del linguaggio. A partire dalla definizione dello standard SQL:1999 molte di queste estensioni  sono state formalmente adottate come parte integrante di SQL nella  sezione SQL/PSM dello standard.

Alcune delle critiche più frequenti rivolte ad SQL riguardano la mancanza di [portabilità](https://it.wikipedia.org/wiki/Portabilità) del codice fra vendor diversi, il modo inappropriato con cui vengono  trattati i dati mancanti (Null), e la semantica a volte inutilmente  complicata.



## PostgreSQL - concetti di Base

<img title="" src="01_Introduzione.assets/2021-12-26-16-19-13-image.png" alt="" width="272" data-align="inline">           

PostgreSQL è un potente database relazionale  ad oggetti, di classe enterprise distribuito con licenza Open Source. Il suo sviluppo è attivo da oltre 15 anni e la sua architettura si è guadagnata una forte reputazione per l'affidabilità e l'integrità dei dati.

PostgreSQL è stato sviluppato originariamente su UNIX ma oggi è disponibile per tutti i principali sistemi operativi. E' testato sulle principali distribuzioni Linux, su UNIX (AIX, BSD, HP-UX, SGI IRIX, Mac OS X, Solaris, Tru64), e Windows.

PostgreSQL non è controllata da nessuna ditta privata o corporation ed il codice sorgente è distribuito liberamente e gratuitamente. Il progetto coinvolge un paio di dozzine di aziende che supportano i contributori di PostgreSQL o contribuiscono direttamente con progetti aziendali al nostro repository. 

E' rilasciato sotto secondo la PostgreSQL License, simile alla licenza BSD o alla licenza MIT.

Non è un sostituto di database desktop (quali ad es. Microsoft Access), ma piuttosto di Oracle, MySQL e prodotti similari.

https://opensource.org/licenses/postgresql

---

### Storia di PostgreSQL

|                                                              |                                                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![image-20220115104549240](01_Introduzione.assets/storia1.png) | Nel **1986** la Defense Advanced Research Projects Agency (DARPA), l'Army Research Office (ARO), il National Science Foundation (NSF), sponsorizzarono l'università di Berkeley per lo sviluppo di del progetto POSTGRES guidato da Michael Stonebraker. il progetto inizialmente venne chiamato INGRES (**IN**telligent **G**raphic **RE**lational **S**ystem) e poi nel [1985](https://it.wikipedia.org/wiki/1985) il progetto prese nuova vita e venne chiamato prima post-Ingres e poi **Postgres** |
| ![image-20220115104621667](01_Introduzione.assets/image-20220115104621667.png) | Nel **1994**, dopo diverse fasi di sviluppo, viene aggiunto il supporto per un interprete al linguaggio SQL e in seguito viene rilasciato sul web sotto il nome di Postgres95. |
| ![image-20220115104632737](01_Introduzione.assets/image-20220115104632737.png) | Dal **1996**, il progetto viene ribattezzato PostgreSQL.     |
| ![image-20220115104640332](01_Introduzione.assets/image-20220115104640332.png) | **Oggi** è disponibile la versione 16 di PostgreSQL (Novembre, 2023). Vengono comunque ancora manutenute le versioni fino alla 12 |



![img](01_Introduzione.assets/2s49cakhod6fs32esuzsjqrpdxv1fj1.png)

Ci sono migliaia di persone che contribuiscono all'ecosistema PostgreSQL. Più di 700 sviluppatori lavorano sul software del database di base, oltre ad un ampia comunità che si occupa della documentazione, delle traduzioni, delle conferenze, della gestione del sito e del supporto. Ci sono anche molti altri progetti associati a PostgreSQL, inclusi driver, librerie, estensioni e altro

Secondo il "2021 Developers Survey" di StackOverflow, PostgreSQL è il secondo database più utilizzato dopo MySQL e sta aumentando costantemente.Mentre solo il 26% circa degli sviluppatori lo utilizzava nel 2017, era già il 34% nel 2019 e nel 2021 era addirittura superiore al 40%.

Viene stimato che circa il 30% delle compagnie tecnologiche usano PostgreSQL per applicazioni core (2012).

La versione 8.0 ha avuto circa un milione di download dopo sette mesi dal rilascio

Molti indici, come il DB-Engines Ranking, dimostrano che l'adozione di PostgreSQL continua a crescere a un ritmo rapido, incluso il riconoscimento di PostgreSQL da parte di DB-Engines come DBMS dell'anno nel 2017, 2018 e 2020. 

La mailing list internazionale conta circa 35,000 iscritti

![](01_Introduzione.assets/2021-12-26-16-23-10-image.png)

---

### Funzionalità

PostgreSQL offre moltissime delle funzionalità avanzate messe a disposizione dai più blasonati database commerciali:

- Profilazione degli utenti e degli accessi

- Tipi dati definiti dall'utente

- Ereditarietà delle tavole

- Foreign key integrità referenziale

- Viste, regole, subquery

- Replica asincrona

- Trigger

- Tablespaces

- Point-in-time recovery

[SQL Feature Comparison](http://www.sql-workbench.net/dbms_comparison.html)

---

### MVCC -  Multiversion concurrency control (Multiutenza)

Il Multiversion concurrency control (MVCC) permette di gestire l'accesso contemporaneo concorrente. L'azione di ogni utente non è visibile agli altri fino a quando la transazione non è terminata.

![](01_Introduzione.assets/2021-12-26-16-45-06-image.png)



### Scalabilità

A partire dalla versione 8, PostgreSQL ha ampiamente migliorato la sua scalabilità e le sue performance

![](01_Introduzione.assets/2021-12-26-16-45-32-image.png)

### Alta disponibilità, Load Balancing, e Replica

In caso di failover è possile promuovere uno degli **hot standby** a master server

![](01_Introduzione.assets/image-20220115135509405.png)

### Linguaggi procedurali

PostgreSQL supporta diversi linguaggi procedurali che permetono all'utente di scrivere codice personalizzato che viene eseguito direttamente dal database server:

- PL/pgSQL,

- PL/Tcl,
- PL/Perl
- PL/Python

Pacchetti esterni permettono il supporto di altri linguaggi non standard:

- PL/PHP,

- PL/V8,
- PL/Ruby,
- PL/Java etc.
- PL/R

### Indici

PostgreSQL supporta nativamente diversi tipi di indici:

- B+-tree,
- hash,
- generalized search trees (GiST) e
- generalized inverted indexes (GIN).

Possono essere anche creati indici personalizzati dagli utenti.

### Triggers
E' possibile eseguire azioni personalizzate, generalmente scatenate od ogni INSERT o UPDATE ceh permettono di personalizzare in modo estremamente versatile il proprio database

### Viste e regole
PostgreSQL supporta le viste, le tavole virtuali e le regole.

### Automatizzazione dei processi

Contestualmente alla digitalizzazione di un poligono è possibile ricavare immediatamente diverse  informazioni. Ad esempio nel caso della  banca dati sulle frane vengono calcolati in automatico, all'inserimento di ogni singola geometria:

- Statistiche di base dati interferometrici
- Elementi antropici che intersecano o che si trovano in prossimità del perimetro di frana
- Dati di base utili alla caratterizzazione del fenomeno (sondaggi, monitoraggi, uso del suolo, pluviometri ecc..)

### Tipi di dati

Sono supportati nativamente moltissimi tipi di dati: Boolean, Arbitrary precision numeric, Character (text, varchar, char), Binary, Date/time (timestamp/time with/without timezone, date, interval), Money Enum, Bit strings, Text search type, Composite Variable length arrays (inclusi testo e tipi compositi), primitive geometriche, indirizzi IPv4 e IPv6, blocchi CIDR e indirizzi MAC, XML che supporta query XPath, UUID.

E’ possibile creare tipi dati, oggetti e funzioni personalizzate come: Casts, Conversions, Data types, Domains, Funzioni comprese le funzioni di aggregazione e window functions, indici che includono indici personalizzati per tipi personalizzati Operatori

### Estensioni

PostgreSQL è costruito in modo da permettere l’aggiunta di nuovi tipo di estensioni in modo relativamente facile. Grazie alle estensioni possono essere installate nuove funzionalità. Possono essere definiti tipi di dati indici, linguaggi e funzioni personalizzate e distribuite tramite le estensioni.

Alcune delle estensioni attualmente disponibili:

| Extension Name                   | Description                                                  |
| -------------------------------- | ------------------------------------------------------------ |
| address_standardizer             | Used to parse an address into constituent elements. Generally used to support geocoding address normalization step |
| address_standardizer_data_us     | Address Standardizer US dataset example                      |
| bloom                            | Bloom access method - signature file based index             |
| btree_gin                        | Support for indexing common datatypes in GIN                 |
| btree_gist                       | Support for indexing common datatypes in GiST                |
| chkpass                          | Data type for auto-encrypted passwords                       |
| citext                           | Data type for case-insensitive character strings             |
| cube                             | Data type for multidimensional cubes                         |
| **dblink**                       | Connect to other PostgreSQL databases from within a database |
| dict_int                         | Text search dictionary template for integers                 |
| earthdistance                    | Calculate great-circle distances on the surface of the Earth |
| fuzzystrmatch                    | Determine similarities and distance between strings          |
| hll                              | Algorithm for the count-distinct problem, approximating the number of distinct elements in a multiset |
| hstore                           | Data type for storing sets of (key                           |
| intagg                           | Integer aggregator and enumerator (obsolete)                 |
| intarray                         | Functions                                                    |
| isn                              | Data types for international product numbering standards     |
| ltree                            | Data type for hierarchical tree-like structures              |
| pg_buffercache                   | Examine the shared buffer cache                              |
| pg_cron                          | Job scheduler for PostgreSQL                                 |
| pg_partman (PostgreSQL 10 only)  | Extension to manage partitioned tables by time or ID         |
| pg_prometheus (PostgreSQL 10-12) | Prometheus metrics for PostgreSQL. The pg_prometheus extension has  been sunset by Timescale in favor of promscale and is not supported for  PostgreSQL 13. |
| pg_repack                        | Reorganize tables in PostgreSQL databases with minimal locks |
| pg_stat_statements               | Track execution statistics of all SQL statements executed    |
| pg_trgm                          | Text similarity measurement and index searching based on trigrams |
| pgcrypto                         | Cryptographic functions                                      |
| **pgrouting**                    | **pgRouting Extension**                                      |
| pgrowlocks                       | Show row-level locking information                           |
| pgstattuple                      | Show tuple-level statistics                                  |
| plcoffee                         | PL/CoffeeScript (v8) trusted procedural language             |
| plls                             | PL/LiveScript (v8) trusted procedural language               |
| plperl                           | PL/Perl procedural language                                  |
| plperlu                          | PL/PerlU untrusted procedural language                       |
| plpgsql                          | PL/pgSQL procedural language                                 |
| plv8 (PostgreSQL 10 only)        | PL/JavaScript (v8) trusted procedural language               |
| **postgis**                      | **PostGIS geometry**                                         |
| **postgis_legacy**               | **Legacy functions for PostGIS**                             |
| **postgis_sfcgal**               | **PostGIS SFCGAL functions**                                 |
| **postgis_tiger_geocoder**       | **PostGIS tiger geocoder and reverse geocoder**              |
| **postgis_topology**             | **PostGIS topology spatial types and functions**             |
| postgres_fdw                     | Foreign-data wrapper for remote PostgreSQL servers           |
| rum                              | RUM index access method                                      |
| sslinfo                          | Information about SSL certificates                           |
| tablefunc                        | Functions that manipulate whole tables                       |
| timescaledb                      | Enables scalable inserts and complex queries for time-series data |
| tsearch2                         | Provides backwards-compatible text search functionality      |
| tsm_system_rows                  | TABLESAMPLE method which accepts number of rows as a limit   |
| unaccent                         | Text search dictionary that removes accents                  |
| unit                             | SI units extension                                           |
| uuid-ossp                        | Generate universally unique identifiers (UUIDs)              |
| wal2json                         | Logical decoding extension                                   |



### PostGIS

PostGIS è un’estensione di PostgreSQL che consente la gestione e l’analisi di dati geografici all’interno del database.

PostGIS aggiunge a PostgreSQL i seguenti oggetti:
- nuovi **tipi di dato**: **geometry**, **geography** e **raster**;
- un certo numero di **funzioni** per la manipolazione di dati geometrici;
- un meccanismo per l’**indicizzazione spaziale**, per recuperare con efficienza i dati geometrici.

- **geometry**: Dati vettoriali basati sul modello del piano (proiezione dello sferoide sul piano)

- **geography**: Dati vettoriali basati sul modello della sfera. Il modello della sfera necessita una matematica molto più complessa e non tutte le funzioni PostGIS lo supportano, inoltre è limitato al sistema di riferimento WGS84. Tuttavia per alcuni calcoli (es. le rotte aeree transoceaniche) il modello della sfera produce risultati accurati mentre la geometria proiettata introduce errori grossolani.
- **raster**: gestione dei dati di tipo raster. L'integrazione dei raster in PostGIS e QGIS è stata decisamente migliorata negli ultimi anni.

Siccome PostGIS è una estensione di PostgreSQL, automaticamente eredita le sue importanti caratteristiche enterprise: garanzia di transazioni ACID  [https://it.wikipedia.org/wiki/ACID], affidabile, crash recovery, hot backup, supporto SQL92 completo, tra le altre.

**Ultima versione disponibile 3.2.2**

PostGIS è diverso da ArcGIS Server perché non è uno strato di astrazione sopra il DB, ma una serie di funzioni inserite direttamente in esso.

## Quale DBMS scegliere

|                                                              | Database            | Carateristiche                                               |
| ------------------------------------------------------------ | ------------------- | ------------------------------------------------------------ |
| ![image-20220115112113046](01_Introduzione.assets/image-20220115112113046.png) | Geopackage / SQLITE | Semplice<br />Non necessità installazione <br/>Può essere distribuito su file <br/>Tipi dati e linguaggio limitati<br/>Accesso a utente singolo |
| ![image-20220115112119073](01_Introduzione.assets/image-20220115112119073.png) | PostgreSQL/PostGIS  | Estremamente potente, flessibile e scalabile<br/>Installazione server<br/>Accesso multiutente<br/>Accesso via web |



## Com'è strutturato un DB PostgreSQL

Su un singolo server è possibile installare più istanze di PostgreSQL (**Database cluster**) che possono avere diverse versioni e sono completamente isolate tra di loro. Ogni Database viene esposto su una porta differente. La porta di default per il primo database è la 5432.

Ogni **database cluster** può contenere più **database**, anch'essi isolati tra di loro. E possibile collegare le tabelle di diversi database utilizzando delle speciali funzioni, generalmente poco comode e prestanti (dblink o foreing data wrapper).

Ogni database può contenere più **schemi**, assimilabili alle cartelle all'interno delle quali vengono organizzati i diversi oggetti: tavole, query, funzioni, tipi dati, procedure, trigger ecc....

Deve sempre essere presente uno **schema public**, che contiene gli oggetti di sistema e che deve essere sempre accessibile a tutti gli utenti. Lo schema public può anche essere utilizzato per contenere i dati degli utenti, ma è consigliabile organizzare il database per schemi differenti, in modo da avere maggior ordine e facilità di amministrazione con il crescere del database.

![image-20220115114851300](01_Introduzione.assets/image-20220115114851300.png)



## Tools per gestire PostgreSQL

Esistono diversi front-end Open Source e proprietari per PostgreSQL.

- **Psql**: gestore da linea di comando. È il principale strumento per accedere a PostgreSQL.
- **PgAdmin4**: La più diffusa GUI per amministrare PostgreSQL e accede ai dati.
- **PhpPgAdmin**: Tool per amministrate PostgreSQL via web, scritto in PHP. Molto utile per amminsitrare database remoti.
- **DBeaver**: GUI multidatabase che permette di connettersi ed interrogare Database: Oracle, PostresSQL, SQLITE, DB3, Access, ecc   

*Altri tool:* Borland Kylix, DBOne, DBTools Manager PgManager, Rekall, Data Architect, SyBase Power Designer, Microsoft Access, eRWin, DeZign for Databases, PGExplorer, Case Studio 2, pgEdit, RazorSQL, MicroOLAP Database Designer, Aqua Data Studio, Tuples, EMS Database Management Tools for PostgreSQL, Navicat, SQL Maestro Group products for PostgreSQL, Datanamic DataDiff for PostgreSQL, Datanamic SchemaDiff for PostgreSQL, DB MultiRun PostgreSQL Edition, SQLPro, SQL Image Viewer, SQL Data Sets etc.



## Accedere a PostgreSQL con PgAdmin

E' Interfaccia grafica libera di riferimento per PostgreSQL. Probabilmente è il programma amministrazione più popolare e ricco di funzioni. Il programma può essere usato su varie piattaforma tra cui Linux, FreeBSD, Solaris, Mac OSX e Windows. È sviluppato da una comunità di specialisti in PostgreSQL ed è disponibile in 12 lingue. È software libero distribuito con la licenza PostgreSQL License.

![image-20220115122721733](01_Introduzione.assets/image-20220115122721733.png)



### Connessione al server

Come prima cosa occorre configurare una connessione al server. In generale servono le seguenti informazioni:

- indirizzo del server
- porta del server
- [database] *in alcuni casi può essere opzionale
- user
- password

Connessione con PGadmin4

Menu: Oggetto -> Registrati

![image-20231115172313095](01_Introduzione.assets/image-20231115172313095.png)



![image-20231115172425183](01_Introduzione.assets/image-20231115172425183.png)

### Keyring

Con PGAdmin è possibile salvare le vostre credenziali di accesso ai database, per proteggere le vostre password vi viene chiesta una master password.

Al primo accesso dovete scegliere la vostra **Master password**, che dovrete inserire tutte le volte seguenti per accedere a PGAdmin.  

![image-20231115172555542](01_Introduzione.assets/image-20231115172555542.png)

## 

## Gestione degli utenti  e dei permessi - introduzione

Una delle principali caratteristiche di un RDBMS è la possibilità di gestire gli accessi con profili diversi a seconda degli utenti. Questo vuol dire che ogni utente potrà avere diritti di lettura/scrittura o altro diversi per ogni oggetto presente nel database.

### Concetti di base

**ROLES, USERS e  GROUPS:** Le versioni precedenti di Postgres e alcuni altri sistemi DB hanno concetti separati di "**gruppi**" (a cui è concesso l'accesso agli oggetti del database) e "**utenti**" (che possono accedere e sono membri di uno o più gruppi).

Nelle versioni moderne di PostgreSQL, i due concetti sono stati fusi: un "**ruolo**" può avere la capacità di accedere, la capacità di "**ereditare**" da altri ruoli (come un utente che è membro di un gruppo o un gruppo che è membro di un altro gruppo) e l'accesso agli oggetti del database.

Per comodità, molti strumenti e manuali fanno riferimento a:

- "**USER**" o "ruolo di accesso" qualsiasi ruolo con autorizzazione di accesso (CAN LOGIN) 

- "**GROUP** " o "ruolo di gruppo", uqalisasi ruolo senza autorizzazione di accesso 

Questa è interamente una convenzione terminologica e per comprendere le autorizzazioni è sufficiente comprendere le opzioni disponibili durante la creazione dei ruoli e la concessione dell'accesso.

**OWNER**: In PostgreSQL, ogni oggetto di database ha solo un **proprietario** che, insieme ai ruoli di *superuser*, ha la capacità di alterare, eliminare e gestire l'oggetto stesso. 

**GRANTS**: Il proprietario dell'oggetto gestisce i privilegi sull'oggetto per altri ruoli concedendo i **privilegi**.

E' possibile profilare i diritti di accesso ai principali oggetti del database. Possono essere forniti i diritti a singoli utenti (**USERS**) o a gruppi (**ROLES**). Ogni oggetto ha differenti tipi di diritti:

DATABASE: Create, Temp, Connect
SCHEMA: Usage, Connect
TABLE, VIEW: Insert, Select, Update, Delete, Truncate, Trigger, References
FUNCTION: Execute

Dopo l'installazione esiste solo l’utente **postgres** che ha diritti di superutente. Si può usare questo utente per amministrare il database oppure crearne un altro con minori privilegi.

L'utente postgres <u>deve essere utilizzato unicamente per effettuare operazioni di amministrazione sul database</u> è altamente sconsigliabile utilizzarlo per l'accesso con altri scopi.  

```sql
CREATE USER utente PASSWORD '***';
--equivale a:
CREATE ROLE utente LOGIN PASSWORD '***';
```

per cambiare o aggiungere la password in un secondo momento:

```sql
ALTER ROLE utente password '***';
```

Per rimuoverlo:

```sql
DROP ROLE utente;
```

E' possibile creare dei gruppi a cui vengono assegnati gli utenti

```sql
CREATE ROLE gruppo;
```

Si aggiungono (e tolgono) gli utenti al gruppo:

```sql
GRANT gruppo TO utente1, utente2, ...;
REVOKE gruppo FROM utente;
```



## Creare database e schemi

In genere dopo l'installazione di base sono presenti alcuni database:

- postgres -> Database di amministrazione, deve sempre essere presente
- template0 -> non deve mai essere modificato per avere un template "pulito"
- template1 -> template utilizzato per la creazione dei nuovi DB

Per creare un nuovo database:

```sql
CREATE DATABASE test OWNER utente;
```

Quando viene creato un nuovo database viene semplicemente creata una copia del db template1.

Se vogliamo utilizzate template personalizzati possiamo usare il comando

```sql
CREATE DATABASE test OWNER utente TEMPLATE mytemplate;
```

All'interno di ogni database è possibile creare nuovi schemi e fornire i grant di accesso.

```sql
CREATE SCHEMA gis;
GRANT USAGE ON SCHEMA gis TO [user];
ALTER DATABASE corso SET search_path = gis, public;
```

L'ultimo comando indica l'ordine con cui vengono cercate le tavole negli schemi. (Opzionale, di default ogni database ha search_path = public ).

## Accesso agli schemi

Un database contiene uno o più schemi, che a loro volta contengono tabelle. Gli schemi contengono anche altri tipi di oggetti, inclusi tipi di dati, funzioni e operatori. Lo stesso nome oggetto può essere utilizzato in schemi diversi senza conflitti; ad esempio, sia schema1 che myschema possono contenere tabelle denominate mytable. 

A differenza dei database, gli schemi non sono rigidamente separati: un utente può accedere agli oggetti in uno qualsiasi degli schemi nel database a cui è connesso, se dispone dei privilegi per farlo.

Esistono diversi motivi per cui si potrebbe voler utilizzare gli schemi:

- Per consentire a molti utenti di utilizzare un database senza interferire tra loro.
- Organizzare gli oggetti del database in gruppi logici per renderli più gestibili.
- Le applicazioni di terze parti possono essere inserite in schemi separati in modo che non entrino in conflitto con i nomi di altri oggetti.

Gli schemi sono analoghi alle directory a livello di sistema operativo, tranne per il fatto che gli schemi non possono essere nidificati.

Per accedere ad un oggetto all'interno di un schema si può usare la sintassi [schema].[oggetto]. Ad esempio per accedere all'oggetto mytable nello schema schema1 la query sarà la seguente:

```SQL
SELECT schema1.mytable
```

se si omette il nome dello schema PostgreSQL cercherà l'oggetto seguendo le regole fissate dalla variabile search_path

```sql
show search_path ; 
  search_path   
\----------------- 
 "$user", public 
(1 row) 
```

La variabile può essere modificata con il comando

```sql
SET search_path = myschema, public 
```

## Naming convention 

PostgreSQL adotta un convenzione particolare per la gestione dei nomi degli oggetti. Questa *naming convention* è valida per tutti gli oggetti di PostgreSQL: (tabelle, viste, colonne ecc...)

- I nomi in PostgreSQL devono iniziare con una lettera minuscola (a-z) o un carattere di sottolineatura (_).
- I caratteri successivi in un nome possono essere lettere, cifre (0-9) o caratteri di sottolineatura.
- I nomi contenenti altri caratteri diversi da quelli precedenti devono essere formati racchiudendoli tra doppi apici (“). Ad esempio, i nomi di tabelle o colonne possono contenere altri caratteri non consentiti come spazi, e commerciale (&), ecc., possiamo usare questi caratteri se citati.
- PostgreSQL memorizza tutti i nomi di tabelle e colonne in minuscolo. Se eseguiamo una select con lettere maiuscole su PostgreSQL, la query fallirà dicendo che la colonna o la tabella non esiste.

I nomi quotati distinguono tra maiuscole e minuscole, mentre i nomi non quotati sono sempre forzati in minuscolo. Ad esempio: 

```sql
CREATE TABLE DESCRIZIONE (gid serial);
CREATE TABLE descrizione (gid serial);
CREATE TABLE "descrizione" (gid serial);
CREATE TABLE "Descrizione" (gid serial);
CREATE TABLE "DESCRIZIONE" (gid serial);
```


- I doppi apici possono anche essere utilizzati per proteggere un nome che altrimenti verrebbe considerato una parola chiave SQL. I nomi di tabelle o colonne non possono essere una parola chiave riservata di PostgreSQL, come USER o PLACING, ecc. Ad esempio, IN è una parola chiave ma "IN" è considerato un nome.

- I nomi in Postgres devono essere univoci all'interno di ciascun tipo di ciascun oggetto. Non possono essere uguali a un altro oggetto PostgreSQL che ha lo stesso tipo.
```sql
CREATE TABLE prova;
CREATE VIEW prova;
```

- La lunghezza dei nomi di default in PostgreSQL è di 31 charatteri. Possiamo fornire nomi più lunghi nelle query, ma verranno troncati. Per impostazione predefinita, NAMEDATALEN è 32, quindi la lunghezza massima del nome è 31 ma al momento della compilazione del sistema, NAMEDATALEN può essere modificato in src/include/postgres_ext.h.

**ATTENZIONE: È consigliabile che i nomi di tutti gli oggetti (campi, tabelle, ecc.) siano minuscoli. Se così non è, in ogni query vanno racchiusi fra virgolette.**

