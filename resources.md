# Risorse disponibili on line


## QGIS
QGIS https://www.qgis.org/it/site/

QGIS e PostGIS tutorial https://docs.qgis.org/3.16/it/docs/training_manual/spatial_databases/index.html

## Admin IDE
pgadmin4 https://www.pgadmin.org/

phpPgadmin https://github.com/phppgadmin/phppgadmin/

psql

## Tutorial
https://www.postgresqltutorial.com/

## Libri / ebooks
https://www.packtpub.com/product/mastering-postgresql-13-fourth-edition/9781800567498

https://www.amazon.com/dp/1796506044

https://www.manning.com/books/postgis-in-action-third-edition
