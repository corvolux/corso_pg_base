---
author: Luca Lanteri/Rocco Pispico
title: Corso PostgreSQL / PostGIS
date: 15 Gennaio, 2022
---

 <img title="" src="00_loghi.assets/logo.png" alt="logo master" data-align="inline">

# Corso PostgreSQL / PostGIS

## Introduzione

<img title="" src="02_Introduzione.assets/2021-12-26-16-19-13-image.png" alt="" width="272" data-align="inline">           <img title="" src="02_Introduzione.assets/2021-12-26-16-20-55-image.png" alt="" data-align="inline">

PostgreSQL è un potente database relazionale  ad oggetti, di classe enterprise distribuito con licenza Open Source. Il suo sviluppo è attivo da oltre 15 anni e la sua architettura si è guadagnata una forte reputazione per l'affidabilità e l'integrità dei dati.

PostgreSQL è stato sviluppato originariamente su UNIX ma oggi è disponibile per tutti i principali sistemi operativi. E' testato sulle principali distribuzioni Linux, su UNIX (AIX, BSD, HP-UX, SGI IRIX, Mac OS X, Solaris, Tru64), e Windows.

PostgreSQL non è controllata da nessuna ditta privata o corporation ed il codice sorgente è distribuito liberamente e gratuitamente. Il progetto coinvolge un paio di dozzine di aziende che supportano i contributori di PostgreSQL o contribuiscono direttamente con progetti aziendali al nostro repository. 

E' rilasciato sotto secondo la PostgreSQL License, simile alla licenza BSD o alla licenza MIT.

Non è un sostituto di database desktop (quali ad es. Microsoft Access), ma piuttosto di Oracle, MySQL e prodotti similari.

https://opensource.org/licenses/postgresql

---

## Storia

|                                                              |                                                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![image-20220115104549240](02_Introduzione.assets/storia1.png) | Nel **1986** la Defense Advanced Research Projects Agency (DARPA), l'Army Research Office (ARO), il National Science Foundation (NSF), sponsorizzarono l'università di Berkeley per lo sviluppo di del progetto POSTGRES guidato da Michael Stonebraker. il progetto inizialmente venne chiamato INGRES (**IN**telligent **G**raphic **RE**lational **S**ystem) e poi nel [1985](https://it.wikipedia.org/wiki/1985) il progetto prese nuova vita e venne chiamato prima post-Ingres e poi **Postgres** |
| ![image-20220115104621667](02_Introduzione.assets/image-20220115104621667.png) | Nel **1994**, dopo diverse fasi di sviluppo, viene aggiunto il supporto per un interprete al linguaggio SQL e in seguito viene rilasciato sul web sotto il nome di Postgres95. |
| ![image-20220115104632737](02_Introduzione.assets/image-20220115104632737.png) | Dal **1996**, il progetto viene ribattezzato PostgreSQL.     |
| ![image-20220115104640332](02_Introduzione.assets/image-20220115104640332.png) | **Oggi** è disponibile la versione 15 di PostgreSQL (Ottobre, 2022). Vengono comunque ancora manutenute le versioni 9 |

Ci sono migliaia di persone che contribuiscono all'ecosistema PostgreSQL. Più di 700 sviluppatori lavorano sul software del database di base, oltre ad un ampia comunità che si occupa della documentazione, delle traduzioni, delle conferenze, della gestione del sito e del supporto. Ci sono anche molti altri progetti associati a PostgreSQL, inclusi driver, librerie, estensioni e altro

Secondo il "2021 Developers Survey" di StackOverflow, PostgreSQL è il secondo database più utilizzato dopo MySQL e sta aumentando costantemente.Mentre solo il 26% circa degli sviluppatori lo utilizzava nel 2017, era già il 34% nel 2019 e nel 2021 era addirittura superiore al 40%.

Viene stimato che circa il 30% delle compagnie tecnologiche usano PostgreSQL per applicazioni core (2012).

La versione 8.0 ha avuto circa un milione di download dopo sette mesi dal rilascio

Molti indici, come il DB-Engines Ranking, dimostrano che l'adozione di PostgreSQL continua a crescere a un ritmo rapido, incluso il riconoscimento di PostgreSQL da parte di DB-Engines come DBMS dell'anno nel 2017, 2018 e 2020. 

La mailing list internazionale conta circa 35,000 iscritti

![](02_Introduzione.assets/2021-12-26-16-23-10-image.png)

---

## Funzionalità

PostgreSQL offre moltissime delle funzionalità avanzate messe a disposizione dai più blasonati database commerciali:

- Profilazione degli utenti e degli accessi

- Tipi dati definiti dall'utente

- Ereditarietà delle tavole

- Foreign key integrità referenziale

- Viste, regole, subquery

- Replica asincrona

- Trigger

- Tablespaces

- Point-in-time recovery

[SQL Feature Comparison](http://www.sql-workbench.net/dbms_comparison.html)

---

## MVCC -  Multiversion concurrency control (Multiutenza)

Il Multiversion concurrency control (MVCC) permette di gestire l'accesso contemporaneo concorrente. L'azione di ogni utente non è visibile agli altri fino a quando la transazione non è terminata.

![](02_Introduzione.assets/2021-12-26-16-45-06-image.png)



## Scalabilità

A partire dalla versione 8, PostgreSQL ha ampiamente migliorato la sua scalabilità e le sue performance

![](02_Introduzione.assets/2021-12-26-16-45-32-image.png)

## Alta disponibilità, Load Balancing, e Replica

In caso di failover è possile promuovere uno degli **hot standby** a master server

![](02_Introduzione.assets/image-20220115135509405.png)

## Linguaggi procedurali

PostgreSQL supporta diversi linguaggi procedurali che permetono all'utente di scrivere codice personalizzato che viene eseguito direttamente dal database server:

- PL/pgSQL,

- PL/Tcl,
- PL/Perl
- PL/Python

Pacchetti esterni permettono il supporto di altri linguaggi non standard:

- PL/PHP,

- PL/V8,
- PL/Ruby,
- PL/Java etc.
- PL/R

## Indici

PostgreSQL supporta nativamente diversi tipi di indici:

- B+-tree,
- hash,
- generalized search trees (GiST) e
- generalized inverted indexes (GIN).

Possono essere anche creati indici personalizzati dagli utenti.

## Triggers
E' possibile eseguire azioni personalizzate, generalmente scatenate od ogni INSERT o UPDATE ceh permettono di personalizzare in modo estremamente versatile il proprio database

## Viste e regole
PostgreSQL supporta le viste, le tavole virtuali e le regole.

## Automatizzazione dei processi

Contestualmente alla digitalizzazione di un poligono è possibile ricavare immediatamente diverse  informazioni. Ad esempio nel caso della  banca dati sulle frane vengono calcolati in automatico, all'inserimento di ogni singola geometria:

- Statistiche di base dati interferometrici
- Elementi antropici che intersecano o che si trovano in prossimità del perimetro di frana
- Dati di base utili alla caratterizzazione del fenomeno (sondaggi, monitoraggi, uso del suolo, pluviometri ecc..)

## Tipi di dati

Sono supportati nativamente moltissimi tipi di dati: Boolean, Arbitrary precision numeric, Character (text, varchar, char), Binary, Date/time (timestamp/time with/without timezone, date, interval), Money Enum, Bit strings, Text search type, Composite Variable length arrays (inclusi testo e tipi compositi), primitive geometriche, indirizzi IPv4 e IPv6, blocchi CIDR e indirizzi MAC, XML che supporta query XPath, UUID.

E’ possibile creare tipi dati, oggetti e funzioni personalizzate come: Casts, Conversions, Data types, Domains, Funzioni comprese le funzioni di aggregazione e window functions, indici che includono indici personalizzati per tipi personalizzati Operatori

## Estensioni

PostgreSQL è costruito in modo da permettere l’aggiunta di nuovi tipo di estensioni in modo relativamente facile. Grazie alle estensioni possono essere installate nuove funzionalità. Possono essere definiti tipi di dati indici, linguaggi e funzioni personalizzate e distribuite tramite le estensioni.

Alcune delle estensioni attualmente disponibili:

| Extension Name                   | Description                                                  |
| -------------------------------- | ------------------------------------------------------------ |
| address_standardizer             | Used to parse an address into constituent elements. Generally used to support geocoding address normalization step |
| address_standardizer_data_us     | Address Standardizer US dataset example                      |
| bloom                            | Bloom access method - signature file based index             |
| btree_gin                        | Support for indexing common datatypes in GIN                 |
| btree_gist                       | Support for indexing common datatypes in GiST                |
| chkpass                          | Data type for auto-encrypted passwords                       |
| citext                           | Data type for case-insensitive character strings             |
| cube                             | Data type for multidimensional cubes                         |
| **dblink**                       | Connect to other PostgreSQL databases from within a database |
| dict_int                         | Text search dictionary template for integers                 |
| earthdistance                    | Calculate great-circle distances on the surface of the Earth |
| fuzzystrmatch                    | Determine similarities and distance between strings          |
| hll                              | Algorithm for the count-distinct problem, approximating the number of distinct elements in a multiset |
| hstore                           | Data type for storing sets of (key                           |
| intagg                           | Integer aggregator and enumerator (obsolete)                 |
| intarray                         | Functions                                                    |
| isn                              | Data types for international product numbering standards     |
| ltree                            | Data type for hierarchical tree-like structures              |
| pg_buffercache                   | Examine the shared buffer cache                              |
| pg_cron                          | Job scheduler for PostgreSQL                                 |
| pg_partman (PostgreSQL 10 only)  | Extension to manage partitioned tables by time or ID         |
| pg_prometheus (PostgreSQL 10-12) | Prometheus metrics for PostgreSQL. The pg_prometheus extension has  been sunset by Timescale in favor of promscale and is not supported for  PostgreSQL 13. |
| pg_repack                        | Reorganize tables in PostgreSQL databases with minimal locks |
| pg_stat_statements               | Track execution statistics of all SQL statements executed    |
| pg_trgm                          | Text similarity measurement and index searching based on trigrams |
| pgcrypto                         | Cryptographic functions                                      |
| **pgrouting**                    | **pgRouting Extension**                                      |
| pgrowlocks                       | Show row-level locking information                           |
| pgstattuple                      | Show tuple-level statistics                                  |
| plcoffee                         | PL/CoffeeScript (v8) trusted procedural language             |
| plls                             | PL/LiveScript (v8) trusted procedural language               |
| plperl                           | PL/Perl procedural language                                  |
| plperlu                          | PL/PerlU untrusted procedural language                       |
| plpgsql                          | PL/pgSQL procedural language                                 |
| plv8 (PostgreSQL 10 only)        | PL/JavaScript (v8) trusted procedural language               |
| **postgis**                      | **PostGIS geometry**                                         |
| **postgis_legacy**               | **Legacy functions for PostGIS**                             |
| **postgis_sfcgal**               | **PostGIS SFCGAL functions**                                 |
| **postgis_tiger_geocoder**       | **PostGIS tiger geocoder and reverse geocoder**              |
| **postgis_topology**             | **PostGIS topology spatial types and functions**             |
| postgres_fdw                     | Foreign-data wrapper for remote PostgreSQL servers           |
| rum                              | RUM index access method                                      |
| sslinfo                          | Information about SSL certificates                           |
| tablefunc                        | Functions that manipulate whole tables                       |
| timescaledb                      | Enables scalable inserts and complex queries for time-series data |
| tsearch2                         | Provides backwards-compatible text search functionality      |
| tsm_system_rows                  | TABLESAMPLE method which accepts number of rows as a limit   |
| unaccent                         | Text search dictionary that removes accents                  |
| unit                             | SI units extension                                           |
| uuid-ossp                        | Generate universally unique identifiers (UUIDs)              |
| wal2json                         | Logical decoding extension                                   |



## PostGIS

PostGIS è un’estensione di PostgreSQL che consente la gestione e l’analisi di dati geografici all’interno del database.

PostGIS aggiunge a PostgreSQL i seguenti oggetti:
- nuovi **tipi di dato**: **geometry**, **geography** e **raster**;
- un certo numero di **funzioni** per la manipolazione di dati geometrici;
- un meccanismo per l’**indicizzazione spaziale**, per recuperare con efficienza i dati geometrici.

- **geometry**: Dati vettoriali basati sul modello del piano (proiezione dello sferoide sul piano)

- **geography**: Dati vettoriali basati sul modello della sfera. Il modello della sfera necessita una matematica molto più complessa e non tutte le funzioni PostGIS lo supportano, inoltre è limitato al sistema di riferimento WGS84. Tuttavia per alcuni calcoli (es. le rotte aeree transoceaniche) il modello della sfera produce risultati accurati mentre la geometria proiettata introduce errori grossolani.
- **raster**: gestione dei dati di tipo raster. L'integrazione dei raster in PostGIS e QGIS è stata decisamente migliorata negli ultimi anni.

Siccome PostGIS è una estensione di PostgreSQL, automaticamente eredita le sue importanti caratteristiche enterprise: garanzia di transazioni ACID  [https://it.wikipedia.org/wiki/ACID], affidabile, crash recovery, hot backup, supporto SQL92 completo, tra le altre.

**Ultima versione disponibile 3.2.2**

PostGIS è diverso da ArcGIS Server perché non è uno strato di astrazione sopra il DB, ma una serie di funzioni inserite direttamente in esso.

## Quale DBMS scegliere

|                                                              | Database            | Carateristiche                                               |
| ------------------------------------------------------------ | ------------------- | ------------------------------------------------------------ |
| ![image-20220115112113046](02_Introduzione.assets/image-20220115112113046.png) | Geopackage / SQLITE | Semplice<br />Non necessità installazione <br/>Può essere distribuito su file <br/>Tipi dati e linguaggio limitati<br/>Accesso a utente singolo |
| ![image-20220115112119073](02_Introduzione.assets/image-20220115112119073.png) | PostgreSQL/PostGIS  | Estremamente potente, flessibile e scalabile<br/>Installazione server<br/>Accesso multiutente<br/>Accesso via web |



## Com'è strutturato un DB PostgreSQL

Su un singolo server è possibile installare più istanze di PostgreSQL (**Database cluster**) che possono avere diverse versioni e sono completamente isolate tra di loro. Ogni Database viene esposto su una porta differente. La porta di default per il primo database è la 5432.

Ogni database cluster può contenere più **database**, anch'essi isolati tra di loro. E possibile collegare le tabelle di diversi database utilizzando delle speciali funzioni, generalmente poco comode e prestanti (dblink o foreing data wrapper).

Ogni database può contenere più **schemi**, assimilabili alle cartelle all'interno delle quali vengono organizzati i diversi oggetti: tavole, query, funzioni, tipi dati, procedure, trigger ecc....

Deve sempre essere presente uno **schema public**, che contiene gli oggetti di sistema e che deve essere sempre accessibile a tutti gli utenti. Lo schema public può anche essere utilizzato per contenere i dati degli utenti, ma è consigliabile organizzare il database per schemi differenti, in modo da avere maggior ordine e facilità di amministrazione con il crescere del database.

![image-20220115114851300](02_Introduzione.assets/image-20220115114851300.png)



## Tools per gestire PostgreSQL

Esistono diversi front-end Open Source e proprietari per PostgreSQL.

**Psql**: gestore da linea di comando. È il principale strumento per accedere a PostgreSQL.

**PgAdmin4**: La più diffusa GUI per amministrare PostgreSQL e accede ai dati.

**PhpPgAdmin**: Tool per amministrate PostgreSQL via web, scritto in PHP. Molto utile per amminsitrare database remoti.

**DBeaver**: GUI multidatabase che permette di connettersi ed interrogare Database: Oracle, PostresSQL, SQLITE, DB3, Access, ecc   



*Altri tool:* Borland Kylix, DBOne, DBTools Manager PgManager, Rekall, Data Architect, SyBase Power Designer, Microsoft Access, eRWin, DeZign for Databases, PGExplorer, Case Studio 2, pgEdit, RazorSQL, MicroOLAP Database Designer, Aqua Data Studio, Tuples, EMS Database Management Tools for PostgreSQL, Navicat, SQL Maestro Group products for PostgreSQL, Datanamic DataDiff for PostgreSQL, Datanamic SchemaDiff for PostgreSQL, DB MultiRun PostgreSQL Edition, SQLPro, SQL Image Viewer, SQL Data Sets etc.

### PHPpgadmin

**phpPgAdmin** è un'applicazione [PHP](https://it.wikipedia.org/wiki/PHP) [libera](https://it.wikipedia.org/wiki/Software_libero) che consente di amministrare in modo semplificato [database](https://it.wikipedia.org/wiki/Database) di [PostgreSQL](https://it.wikipedia.org/wiki/PostgreSQL) tramite un qualsiasi [browser](https://it.wikipedia.org/wiki/Browser). L'applicazione è indirizzata sia agli amministratori del [database](https://it.wikipedia.org/wiki/Database), sia agli utenti. Gestisce i permessi prelevandoli dal database PostgreSQL.
**phpPgAdmin** permette di creare un database da zero, creare le tabelle ed eseguire  operazioni di ottimizzazione sulle stesse. Presenta un feedback sulla  creazione delle tabelle per evitare eventuali errori. Sono previste  delle funzionalità per l'inserimento dei dati (*popolazione del database*), per le query, per il backup dei dati, ecc..

L'amministratore, invece ha a disposizione un'interfaccia grafica per la gestione degli utenti: l'interfaccia permette l'inserimento di  un nuovo utente, la modifica della relativa password e la gestione dei  permessi che l'utente ha sul database, utilizzando lo standard [SQL](https://it.wikipedia.org/wiki/SQL).

Il programma quo essere scaricato da qui. https://www.phpmyadmin.net/downloads/

### DBeaver

E' **un client SQL e uno strumento di amministrazione del database**. È un'applicazione desktop scritta in Java e basata sulla piattaforma  Eclipse. Utilizza le [API JDB](https://en.m.wikipedia.org/wiki/Java_Database_Connectivity)** per interagire con i database tramite un driver JDBC. Per altri database non SQL, utilizza driver di database proprietari.

Questo client fornisce un file **editor che supporta il completamento del codice e l'evidenziazione della sintassi**. Avremo anche un'architettura plug-in basata sull'architettura plug-in  Eclipse, che consentirà agli utenti di modificare gran parte del  comportamento dell'applicazione per ottenere funzioni o caratteristiche  specifiche del database.

![image-20220115114627874](02_Introduzione.assets/image-20220115114627874.png)

Questo software è destinato agli sviluppatori e  supporta non solo Microsoft SQL. Ci permetterà anche di lavorare con  molti altri **database relazionali** popolare come *MySQL, PostreSQL, SQLite, Oracle, DB2, MariaDB, Sybase, Teradata, Netezza, ecc.* Allo stesso tempo ammette alcuni **Database NoSQL** come *[MongoDB](https://ubunlog.com/it/come-installare-il-sistema-di-database-mongodb-su-Ubuntu/), Cassandra, Redis, Apache Hive, ecc*.

È importante menzionare anche questo **c'è una versione a pagamento**, questo ci consentirà di utilizzare NoSQL o se abbiamo bisogno di alcune funzionalità aggiuntive come l'integrazione o l'assistenza di Office.

### PGAdmin4

E' Interfaccia grafica libera di riferimento per PostgreSQL. Probabilmente è il programma amministrazione più popolare e ricco di funzioni. Il programma può essere usato su varie piattaforma tra cui Linux, FreeBSD, Solaris, Mac OSX e Windows. È sviluppato da una comunità di specialisti in PostgreSQL ed è disponibile in 12 lingue. È software libero distribuito con la licenza PostgreSQL License.

![image-20220115122721733](02_Introduzione.assets/image-20220115122721733.png)



Come prima cosa occorre configurare una connessione al server

![image-20220115123005476](02_Introduzione.assets/image-20220115123005476.png)



![image-20220115123018046](02_Introduzione.assets/image-20220115123018046.png)



### QGIS - DBManager

QGIS nasce come visualizzatore di dati per postGIS quindi integra perfettamente in modo nativo la connessione a PostgreSQL.

Il plugin DB Manager (adesso inserito nel core QGIS) è lo strumento principale per integrare e gestire i formati di database  spaziali supportati da QGIS (PostGIS, SpatiaLite, GeoPackage, Oracle  Spatial, Virtual layer) in un’unica interfaccia utente. Il Plugin [![dbManager](02_Introduzione.assets/dbmanager.png)](https://docs.qgis.org/2.18/it/_images/dbmanager.png) DB Manager ha diverse funzionalità. Puoi trascinare i layer da QGIS Browser  direttamente in DB Manager e questo importerà i tuoi layer nel tuo  database spaziale. Puoi spostare tabelle fra diversi database spaziali e le stesse saranno importate.



![](02_Introduzione.assets/image-20220115181640834.png)

Il menu ***Database*** ti permette di  collegarti a un database esistente, di lanciare la finestra SQL e di  uscire dal plugin DB Manager. Una volta che ti sei connesso a un  database esistente, vengono visualizzati anche i menu *Schema* e *Tabella*.

Il menu ***Schema*** include gli strumenti  per creare ed eliminare gli schemi (vuoti) e, se è disponibile la  topologia (ad esempio, PostGIS2) per avviare un *TopoViewer*.

Il menu ***Table*** ti consente di creare e modificare tabelle e di eliminare tabelle e viste. È inoltre possibile  svuotare le tabelle e spostare le tabelle da uno schema all’altro. Come  ulteriore funzionalità, puoi eseguire un VACUUM e quindi un’analisi per  ciascuna tabella selezionata. Il normale VACUUM recupera semplicemente  lo spazio e lo rende disponibile per il riutilizzo. ANALYZE aggiorna le  statistiche per determinare il modo più efficace per eseguire una query. Infine, puoi importare layer/files, se sono caricati in QGIS o esistono nel file system. E puoi esportare le tabelle di database in shapefiles  con la funzione di Esportazione File.

La finestra *Providers* elenca tutti i  database supportati da QGIS. Puoi collegarti al database con un doppio  click. Con il tasto destro del mouse puoi rinominare ed eliminare schemi e tabelle esistenti. Le tabelle possono essere aggiunte nella mappa di  QGIS anche dal menu contestuale.

Se si è connessi a un database, compaiono tre schede nella finestra **principale** di DB Manager. La scheda *Informazioni* fornisce informazioni sulla tabella e sulle geometrie, oltre a campi,  vincoli e indici esistenti. Permette inoltre di effettuare una “Vacuum  Analyze” e di creare un indice spaziale, se non è presente, per la  tabella selezionata. La scheda *Tabella* mostra tutti gli attributi e la scheda *Anteprima* visualizza una anteprima delle geometrie.

E' presente un editor di codice SQL e la possibilità di visualizzare direttamente in QGIS il risultato delle query.

![image-20220115181756254](02_Introduzione.assets/image-20220115181756254.png)



## Naming convention 


La naming convention è valida per tutti gli oggetti di PostgreSQL: (tabelle, viste, colonne ecc...)
- I nomi in PostgreSQL devono iniziare con una lettera (a-z) o un carattere di sottolineatura (_).
- I caratteri successivi in un nome possono essere lettere, cifre (0-9) o caratteri di sottolineatura.
- I nomi contenenti altri caratteri devono essere formati racchiudendoli tra doppi apici (“). Ad esempio, i nomi di tabelle o colonne possono contenere altri caratteri non consentiti come spazi, e commerciale (&), ecc., possiamo usare questi caratteri se citati.
- PostgreSQL memorizza tutti i nomi di tabelle e colonne in minuscolo. Se eseguiamo una select con lettere maiuscole su PostgreSQL, la query fallirà dicendo che la colonna o la tabella non esiste.

I nomi quotati distinguono tra maiuscole e minuscole, mentre i nomi non quotati sono sempre forzati in minuscolo. Ad esempio: 

```sql
CREATE TABLE DESCRIZIONE;
CREATE TABLE descrizione;
CREATE TABLE "descrizione";
CREATE TABLE "Descrizione";
CREATE TABLE "DESCRIZIONE";
```


- I doppi apici possono anche essere utilizzati per proteggere un nome che altrimenti verrebbe considerato una parola chiave SQL. I nomi di tabelle o colonne non possono essere una parola chiave riservata di PostgreSQL, come USER o PLACING, ecc. Ad esempio, IN è una parola chiave ma "IN" è considerato un nome.

- I nomi in Postgres devono essere univoci all'interno di ciascun tipo di ciascun oggetto. Non possono essere uguali a un altro oggetto PostgreSQL che ha lo stesso tipo.
```sql
CREATE TABLE prova;
CREATE VIEW prova;
```

- La lunghezza dei nomi di default in PostgreSQL è di 31 charatteri. Possiamo fornire nomi più lunghi nelle query, ma verranno troncati. Per impostazione predefinita, NAMEDATALEN è 32, quindi la lunghezza massima del nome è 31 ma al momento della compilazione del sistema, NAMEDATALEN può essere modificato in src/include/postgres_ext.h.

**ATTENZIONE: È consigliabile che i nomi di tutti gli oggetti (campi, tabelle, ecc.) siano minuscoli. Se così non è, in ogni query vanno racchiusi fra virgolette.**


## Accesso agli schemi

Un database contiene uno o più schemi, che a loro volta contengono tabelle. Gli schemi contengono anche altri tipi di oggetti, inclusi tipi di dati, funzioni e operatori. Lo stesso nome oggetto può essere utilizzato in schemi diversi senza conflitti; ad esempio, sia schema1 che myschema possono contenere tabelle denominate mytable. 

A differenza dei database, gli schemi non sono rigidamente separati: un utente può accedere agli oggetti in uno qualsiasi degli schemi nel database a cui è connesso, se dispone dei privilegi per farlo.

Esistono diversi motivi per cui si potrebbe voler utilizzare gli schemi:

- Per consentire a molti utenti di utilizzare un database senza interferire tra loro.
- Organizzare gli oggetti del database in gruppi logici per renderli più gestibili.
- Le applicazioni di terze parti possono essere inserite in schemi separati in modo che non entrino in conflitto con i nomi di altri oggetti.

Gli schemi sono analoghi alle directory a livello di sistema operativo, tranne per il fatto che gli schemi non possono essere nidificati.

Per accedere ad un oggetto all'interno di un schema si può usare la sintassi [schema].[oggetto]. Ad esempio per accedere all'oggetto mytable nello schema schema1 la query sarà la seguente:

```SQL
SELECT schema1.mytable
```

se si omette il nome dello schema PostgreSQL cercherà l'oggetto seguendo le regole fissate dalla variabile search_path

```sql
show search_path ; 
  search_path   
\----------------- 
 "$user", public 
(1 row) 
```

La variabile può essere modificata con il comando

```sql
SET search_path = myschema, public 
```