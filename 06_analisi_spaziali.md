---
author: Luca Lanteri/Rocco Pispico
title: Corso PostgreSQL / PostGIS
date: 31 Gennaio, 2022
---

 <img title="" src="00_loghi.assets/logo.png" alt="logo master" data-align="inline">

# Capitolo 6 - Analisi spaziali

## ESERCIZIO 7 - Join (ma con qualche funzione PostGIS)

- da DBManager o da PGAdmin eseguite questa query

```sql
SELECT 
  l.fid, 
  ST_Centroid(l.geom) as geom,
  l.loc2011 as codice_localita, 
  l.pro_com as codice_comune, 
  l.tipo_loc, 
  tl.descrizione as desc_tipo_localita,   
  l.denominazi as nome_localita,
  l.popres as popolazione_residente,
  l.maschi as maschi,
  l.popres - l.maschi as femmine,
  l.famiglie as numero_famiglie,
  l.abitazioni as numero_abitazioni,
  l.edifici as numero_edifici,
  l.shape_area as superficie_in_mq
FROM 
  dati.localita_proc l, 
  dati.tipo_localita tl
WHERE 
  l.tipo_loc::integer = tl.tipo_loc::integer
  limit 10
```

Ci sono alcune novità:
- **ST_Centroid(l.geom) as geom** trasformiamo il layer poligonale in elementi puntuali con la funzione **ST_Centroid**
- creiamo un dato che non esiste nelle tavole utilizzando una semplice formula matematica **l.popres - l.maschi as femmine**
- abbiamo messo tanti _alias_ la tavola è più leggibile
- abbiamo trasformato i campi **tipo_loc** delle due tabelle in **interi**. Il doppio **::** si riferisce a operatori che effettuano di **CAST** ovvero di trasformazioni di formato. https://www.postgresqltutorial.com/postgresql-cast/

---

## Viste

_Le viste sono un elemento utilizzato dalla maggior parte dei DBMS. Si tratta, come suggerisce il nome, di "modi di vedere i dati".
Una vista è rappresentata da una query (SELECT), il cui risultato può essere utilizzato come se fosse una tabella._
https://it.wikipedia.org/wiki/Vista_(basi_di_dati)

Le viste ci danno la possibilità di:
- dare una forma differente alle tavole con più o meno campi o con nomi differenti 
- creare nuove tavole prendendo le informazioni da più tavole relazionate
- il contenuto di una vista è **dinamico**: la vista segue i cambiamenti (contenuto di una o più colonne o aggiunta/rimozione dei record/righe) nelle tavole che alimentano la vista. 

---

## ESERCIZIO 8 - Creare una vista

Trasformiamo la query dell'esercizio precedente in una **vista** con l'aggiunta di questo comando:

**CREATE OR REPLACE VIEW dati.v_centroidi AS** 

```sql
CREATE OR REPLACE VIEW dati.v_centroidi AS 
SELECT 
  l.fid, 
  ST_Centroid(l.geom) as geom,
  l.loc2011 as codice_localita, 
  l.pro_com as codice_comune, 
  l.tipo_loc, 
  tl.descrizione as desc_tipo_localita,   
  l.denominazi as nome_localita,
  l.popres as popolazione_residente,
  l.maschi as maschi,
  l.popres - l.maschi as femmine,
  l.famiglie as numero_famiglie,
  l.abitazioni as numero_abitazioni,
  l.edifici as numero_edifici,
  l.shape_area as superficie_in_mq
FROM 
  dati.localita_proc l, 
  dati.tipo_localita tl
WHERE 
  l.tipo_loc::integer = tl.tipo_loc::integer;
```
**ATTENZIONE**: controllate che il LIMIT sia stato rimosso.

- caricate i due layer in un progetto QGIS, aggiungete uno o più poligoni sul layer delle località.

DOMANDA: cosa accade quando salvate l'editing?

DOMANDA 2: potete fare editing sul layer dei centroidi?

DOMANDA 3: c'è qualcosa di strano nel Browser di QGIS? e nel DBManager?

Controllate la _feature type_ del campo **geom** della vista 

DOMANDA 4: navigate tra i dati con QGIS trovate qualche anomalia?

---

## ESERCIZIO 9 - Centroidi e centroidi

Trasformiamo la query dell'esercizio precedente in una nuova **vista** con l'aggiunta di questo comando:

**CREATE OR REPLACE VIEW dati.v_centroidi_2 AS** 

```sql
CREATE OR REPLACE VIEW dati.v_centroidi_2 AS 
SELECT 
  l.fid, 
  ST_PointOnSurface(l.geom)::geometry(Point,32632) AS geom, 
  l.loc2011 as codice_localita, 
  l.pro_com as codice_comune, 
  l.tipo_loc, 
  tl.descrizione as desc_tipo_localita,   
  l.denominazi as nome_localita,
  l.popres as popolazione_residente,
  l.maschi as maschi,
  l.popres - l.maschi as femmine,
  l.famiglie as numero_famiglie,
  l.abitazioni as numero_abitazioni,
  l.edifici as numero_edifici,
  l.shape_area as superficie_in_mq
FROM 
  dati.localita_proc l, 
  dati.tipo_localita tl
WHERE 
  l.tipo_loc::integer = tl.tipo_loc::integer;
```

- caricate la nuova vista in QGIS

DOMANDA: ci sono differenze nelle geometrie delle due viste?

 <img title="" src="06_analisi_spaziali.assets/centroid.png" alt="centroidi" data-align="inline">

DOMANDA 2: come si presenta il campo **geom** nel Browser di QGIS? e nel DBManager?

---

## Join spaziale

Fino ad ora abbiamo realizzato delle funzioni di **join** tra valori presenti nelle tabelle 

**WHERE l.tipo_loc::integer = tl.tipo_loc::integer**

possiamo utilizzare le funzioni che soddisfano **relazioni topologiche** tra le geometrie e che rispondo alle domande: è contenuto, tocca, interseca, non interseca ... https://postgis.net/docs/reference.html#idm11890

 <img title="" src="06_analisi_spaziali.assets/relazionispaziali.png" alt="relazioni spaziali" data-align="inline">

Notate che queste funzioni restituiscono un valore booleano TRUE o FALSE. 

 <img title="" src="06_analisi_spaziali.assets/msg372662734-23071.jpg" alt="twitter" data-align="inline" style="width:50%">

---

## ESERCIZIO 10 - Select con intersect

- Caricate in Postgres il layer dei comuni2019.gpkg che trovate nella cartella dei dati utilizzando Drag&Drop
- Rinominate la tavola in **comuni_piemonte** con il comando:

```sql
ALTER TABLE IF EXISTS dati.comuni2019 RENAME TO comuni_piemonte;
```

- Eseguite la query

```sql
select 
 c.fid id_comune, l.fid id_localita, c.comune_nom, l.nome_localita, l.numero_famiglie::integer, l.numero_abitazioni
from 
 dati.comuni_piemonte c, 
 dati.v_centroidi_2 l
where 
 ST_Intersects(l.geom, c.geom)
 and c.comune_nom ilike 'Torino';
```

---

## ESERCIZIO 11 - Select con conteggio

- Eseguite la query

```sql
select 
 c.comune_nom || ' (' || c.prov || ')' comune, 
 count(*) as numero_localita
from 
  dati.comuni_piemonte c, 
  dati.localita l
where 
ST_Intersects(c.geom, ST_PointOnSurface(l.geom)) 
and c.prov in ('at', 'bi')  
group by 1
order by 1;
```

Ci sono alcune novità:

- il **||** permette di concatenare delle stringhe 
- usiamo la funzione **ST_PointOnSurface** direttamente nella **ST_Intersects** senza passare da una vista
- la funzione **in** permette di indicare una lista di occorrenze, in questo caso vogliamo filtrare solo le province di Asti e Biella
- nella **group by** e nella **order by** non indichiamo direttamente una colonna di una tavola ma il campo in posizione **1**


DOMANDA: quanto tempo ha impiegato il server a rispondere? 

DOMANDA 2: il layer ha un indice spaziale? Se non lo avesse createlo. 
Riprovate ad eseguire la query quanto tempo è passato per avere la risposta? 

- Modificate la query per avere la colonna **c.prov** in maiuscolo
(cercate una funzione di Postgres che trasforma le stringhe in maiuscolo)

- Modificate la query per avere il risultato in ordine di **numero_localita** dal più grande al più piccolo

---

## ESERCIZIO 12 - Select con conteggio 2

- Eseguite la query

```sql
select 
 c.comune_nom || ' (' || upper(c.prov) || ')' comune, 
 sum(l.maschi) as tot_maschi, 
 sum(l.femmine) as tot_femmine
from 
  dati.comuni_piemonte c, 
  dati.v_centroidi_2 l
where c.prov not in ('at', 'bi')  
and ST_Intersects(c.geom, l.geom) 
group by 1
order by 1;
```

Ci sono alcune novità:

- con **sum(l.maschi) as tot_maschi** e **sum(l.femmine) as tot_femmine** sommiamo i valori di due colonne della vista
- il **not in** permette filtrare solo quei comuni delle province che **non** sono Asti e Biella

CONSIGLIO: nelle clausole **WHERE** mettete sempre prima le condizioni che filtrano il vostro dataset

---

## ESERCIZIO 13 - Select con conteggio 3

- Caricate in Postgres il layer autostrade.shp che trovate nella cartella dei dati

- Eseguite la query

```sql
select 
 c.comune_nom, el_str_sed, 
 count(*) as numero_archi,
 sum(ST_Length(ST_Intersection(c.geom, v.geom))/1000) as lun_km
from 
  dati.comuni_piemonte c, 
  dati.autostrade v
where ST_Intersects(c.geom,v.geom) 
group by c.comune_nom,el_str_sed
order by c.comune_nom,el_str_sed;
```

Ci sono alcune novità:

- la funzione **ST_Intersection** restituisce il tratto in comune tra il comune e l'arco delle autostrade
- la funzione **ST_Length** restituisce la lunghezza dell'arco in metri che poi dividiamo per 1000 per ottenere i km

---

## ESERCIZIO 14 - Trasformazioni 

- Eseguite la query

```sql
select 
s."STRUMENTO", l.nome_localita, 
st_distance(s.geom,  l.geom) d
from dati.staz s, 
dati.v_centroidi_2 l
where ST_Distance(s.geom, l.geom)  <= 3000
```

DOMANDA: cosa accade?

- Proviamo a risolvere con questa query

```sql
create table dati.staz_32632 as 
SELECT id,  
ST_Transform(geom,32632)::Geometry(Point,32632) geom,
--ST_AsText(st_transform(geom,32632)::Geometry(Point,32632)),
"A_INDIRIZZO" a_indirizzo, "STRUMENTO" strumento
FROM dati.staz
```

Ci sono alcune novità:

- creiamo un nuovo layer **dati.staz_32632** utilizzando una **SELECT**
- nella creazione cambiamo di SRID la geometria da 4326 a 32632

---

## ESERCIZIO 15 - Nuove geometrie 

Cosa fa questa query?

```sql
CREATE TABLE dati.distanze AS
SELECT 
s.a_indirizzo, l.nome_localita, l.desc_tipo_localita,
ST_Distance(s.geom,  l.geom) d, 
ST_GeomFromText('LINESTRING (' || st_x(s.geom) || ' ' || st_y(s.geom) || ',' 
                               || st_x(l.geom) || ' ' || st_y(l.geom) || ')',32632) as geom
FROM dati.staz_32632 s, 
dati.v_centroidi_2 l 
WHERE l.desc_tipo_localita ILIKE '%abitato%'
AND ST_Distance(s.geom, l.geom)  <= 2000;
```

Ci sono alcune novità:

- l'operatore **ILIKE** permette di ricercare nelle stringhe usando dei caratteri _jolly_ **%**
In questo caso filtriamo tutte le righe **desc_tipo_localita** che contengono la stringa **abitato**
- la funzione **ST_Distance** la usiamo in due situazioni:
  1. nella clausola **WHERE** per filtrare solo le località che sono ad una distanza inferiore a 2000 metri
  2. nel calcolo della distanza tra la stazione e la località che verrà memorizzato nel campo **d**
- la funzione **ST_GeomFromText** ci permette di creare una nuova geometria a partire dalle coordinate della stazione e della località

## ESERCIZIO 15 Bis - Nuove geometrie 2

Cosa fa questa query?

```sql
CREATE TABLE dati.distanze_autostrade AS
SELECT s.id, 
s.a_indirizzo, 
ST_Distance(s.geom,  a.geom) d, 
ST_GeomFromText('LINESTRING (' || st_x(s.geom) || ' ' || st_y(s.geom) || ',' 
                               || st_x(ST_PointOnSurface(a.geom)) || ' ' || st_y(ST_PointOnSurface(a.geom)) || ')',32632) as geom		
FROM 
dati.staz_32632 s, 
dati.autostrade a 
WHERE ST_Distance(s.geom, ST_PointOnSurface(a.geom))  <= 2000
and a.el_str_sed in ('in galleria','su ponte/viadotto/cavalcavia')
```

Stesso esercizio precedente ma questa volta usiamo il layer delle **autostrade** consideriamo solo il punto centrale dell'arco con la funzione **ST_PointOnSurface(a.geom)**
